\documentclass[titlepage,a4paper]{article}
\pagestyle{headings}


\usepackage[utf8]{inputenc}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{float}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{appendix}
\renewcommand{\appendixpagename}{Apêndices}
\usepackage{minted}

\title{Análise da competição por recursos entre vegetação rasteira e
  arbórea no cerrado brasileiro utilizado autômatos celulares baseados
  na dinâmica ecohidrológica}

\author{
    \textbf{Carlos Eduardo Gallo Filho}\\
    10732350\\
    \texttt{gallocarloseduardo@usp.br}
    \and
    \textbf{Kaio Bernardo de Barros}\\
    10691136\\
    \texttt{kaiobbarros@usp.br}
    \and
}
\date{Junho de 2021}

\begin{document}

\maketitle

\section{Introdução}
O padrão observado em alguns biomas mais secos, como o serrado
brasileiro, é caracterizado pela competição entre árvores e vegetação
rasteira. A proporção observada entre as diferentes populações de
plantas na manutenção deste equilíbrio tem como recurso chave a água,
com a ocorrência de incêndios e pastagem de animais como fatores
secundários~\cite{fensham1999temporal}.

Para a completa descrição desse sistema ecológico, é necessário se
funcionalizar a frequência de chuvas e disponibilidade de água no solo
com a quantidade de vegetação rasteira e
arbórea~\cite{van2002tree}. Utiliza-se com esse fim uma descrição
probabilística da dinâmica hídrica para a formação de uma função
dinâmica de estresse que inclui os períodos de estresse hídrico,
intensidade deste e a frequência de ocorrência de tais períodos
períodos~\cite{rodriguez2001plants}. Esta funcionalização exerce
importância crucial na descrição do evento probabilístico envolvido no
crescimento e morte das vegetações estudadas. Isso irá determinar o
resultado da competição por espaço entre as espécies arbóreas e
rasteiras~\cite{van2002tree}.

Para que uma espécie vegetativa seja capaz de crescer em um ambiente,
ela deve se sair bem em três diferentes aspectos: habilidade de lidar
com adversidades, competir por recursos e utilizá-los
eficientemente~\cite{van2002tree}. Dentre o escopo deste trabalho, a
capacidade de sobrevivência a adversidades e competição serão as
habilidades mais exploradas, visto que o estudo envolve a competição
de espécies em um ambiente que possui recursos muitas vezes escassos,
contando com as épocas de seca.

Para fazer a relação entre os dados utilizados e a evolução
eco-hidrológica do sistema, utilizaremos um modelo baseado em autômatos
celulares como o realizado em~\cite{van2002tree}. A modelagem envolvendo
todos os parâmetros que relacionam a ecologia e a hidrologia é um
tanto quanto complexa, e ao invés disso, a modelagem utilizando
autômatos celulares proporciona uma maneira mais simplifica de se
parametrizar o sistema, mesmo que havendo perda de informações. O
interesse desta modelagem se pauta em apresentar um modelo que seja
plausível e correlacionado ao padrão observado na natureza.

\subsection{Descrição do modelo pontual}
No modelo pontual as árvores e gramíneas competem entre si pelo espaço
livre, tendo suas mortes ou taxa de reprodução diretamente
relacionadas à equação de balanceamento da umidade do solo em um
ponto~\cite{rodriguez1999spatial}, dada por:
\begin{equation}
  n Z_r \frac{ds}{dt} = I(s,t) - E(s,t) - L(s,t)
  \label{eq:pontual}
\end{equation}
onde\\
$n$: porosidade;\\
$Z_r$: profundidade efetiva do solo;\\
$s(t)$: umidade relativa do solo ou nível de saturação;\\
$I(s,t)$: taxa de infiltração da chuva;\\
$E(s,t)$: taxa de evapotranspiração;\\
$L(s,t)$: taxa de infiltração profunda ou de vazamento;\\
Para tal, a chuva é considerado um modelo de Poisson marcado no
tempo, caracterizada pela taxa com a qual ocorre ($\lambda$) e sua
intensidade de média $\alpha^{-1}$~\cite{van2002tree}.

O modelo é construído seguindo as seguintes considerações:
\begin{itemize}
\item A quantidade de água infiltrada no solo é equivalente à
  quantidade total precipitada em uma chuva qualquer, desde que o solo
  esteja com sua capacidade além deste total.
\item Se a chuva exceder a capacidade de armazenamento de água do
  solo, há escoamento.
\item O escoamento ocorre mesmo quando o solo não excede a capacidade
  de armazenamento; neste caso, utiliza-se uma relação com sua
  umidade~\cite{rodriguez1999spatial}. 
\item É estabelecido um limiar constante para a chuva, ou seja, uma
  constante que subtrai o total de quantidade de água que chegou ao
  solo.
\item A transpiração é linearmente dependente da quantidade de água no
  solo até certo ponto, a partir do qual a esta cessa.
\item A transpiração média máxima $E_{\max}$ representa a transpiração
  diária sobre um solo não seco e é constante ao longo da
  estação.
\item Se necessário, os valores de $alpha$ e $\lambda$ podem ser
  considerados separadamente para o começo e final da estação de crescimento.
\end{itemize}

Tendo esta modelagem em mente, a quantificação do estresse dinâmico
hídrico se torna possível e é realizado. Em seguida, estes resultados
são utilizados no modelo espacial, junto de sua devida simulação. O
cálculo do valor de estresse hídrico dinâmico para o solo é descrito
no apêndice A.

\subsection{Descrição do modelo espacial}
No modelo espacial, arbóreas e gramíneas competem por espaço, e não
pela água disponível. A taxa de morte e propagação estão diretamente
ligadas com o valor sazonal de crescimento como função do estresse
dinâmico resultante da modelagem descrita acima.

Um modelo automata celular é definido em uma grade 100 por 100
células, onde o status de cada célula é atualizado a cada instante de
tempo dependendo de sua vizinhança~\cite{rodriguez1999spatial}. Dessa
forma, interações espaciais explícitas podem ser facilmente
incorporadas. As células são consideradas com tamanho de uma árvore
adulta (25 m²). Existem quatro configurações possíveis para uma
célula: pode ser desocupada, ocupada por uma árvore, por uma árvore em
desenvolvimento (idade de 1 a 5 anos) ou por vegetação rasteira. Nesse modelo,
não são possíveis ocupações mistas em cada célula. A árvore em
desenvolvimento não pode reproduzir, e após 5 anos, se torna uma
árvore adulta. Condições de contorno periódicas são assumidas ao longo
da simulação com o intuito de prevenir efeitos de
contorno~\cite{van2002tree}.

Para cada célula, o modelo pontual com sua devida parametrização é
utilizado e retorna o valor de estresse dinâmico da região. O modelo
espacial possui um caráter homogêneo acerca de suas componentes
abióticas, ou seja, toda sua parametrização do solo, chuva e
escoamento é considerada como uma componente única da grade total de
células, significando que essas componentes não irão influenciar no
comportamento único de cada célula, deixando estas unicamente a mercê
dos fatores bióticos considerados: morte e
reprodução~\cite{van2002tree}. Dito isso, este modelo não tem precisão
necessária para reproduzir realisticamente a evolução espaçotemporal
da estrutura de vegetação estudada, e sim explorar o impacto da chuva
a longo prazo em alguns aspectos chaves no padrão de vegetação
rasteira e arbórea no bioma estudado~\cite{van2002tree}.

O cálculo da reprodução e morte da vegetações são modeladas de forma
que as chances da morte de cada ente é dada pelo valor do estresse
dinâmico menos um valor de limiar~\cite{van2002tree}. A utilização de
um valor limiar se trata de uma aproximação da capacidade das plantas
de lidarem com uma quantidade de estresse hídrico sem que se haja
muitos prejuízos, como sua eventual morte, ou seja, estresses hídricos
muito baixos são de certa forma ``irrelevantes'', pois as plantas
conseguem lidar bem com tal através de diversos mecanismos. Há também
incorporado no modelo uma taxa de morte mínima, randômica, que não
possui relação com o nível de estresse dinâmico. Tais parâmetros são
obtidos para todos os tipos de vegetação considerados: árvores,
árvores em desenvolvimento e plantas rasteiras.


A distribuição das plantas em desenvolvimento se dá diferente para
cada classe de plantas. Assume-se que a distribuição de sementes de
árvores é muito mais localizada que a distribuição das sementes da
vegetação rasteira~\cite{jeltsch1996tree}. Uma árvore, neste modelo, é
capaz de semear terrenos sobre uma vizinhança de Moore de raio 2, ou
seja, os 24 terrenos mais próximos. A probabilidade de semeadura é
altamente sensível ao estresse dinâmico, e portanto, esta tem seu
valor decrescido até 0 em um intervalo pequeno de estresse menos
o limiar constante para cada tipo de vegetação; de 0 a 0.1 para as
árvores e de 0 a 0.2 para a vegetação rasteira~\cite{van2002tree}.

O colonização neste modelo se dá da seguinte maneira:
\begin{itemize}
\item A semeadura de cada planta ocorre apenas em células vazias.
\item A vizinhança é constituída pela vizinhança de Moore com raio 2.
\item As células vizinhas são potenciais semeadouras.
\item Uma célula vizinha ocupada por árvore ou vegetação rasteira é escolhida
  aleatoriamente para realizar a semeadura.
\item Se a célula escolhida for ocupada por uma árvore, haverá uma
  probabilidade baseada no estresse dinâmico de se realizar a
  semeadura da célula vazia.
\item Se a célula continua vazia após o calculo ou a célula escolhida
  não é uma árvore, há a possibilidade da semeadura de vegetação
  rasteira, com probabilidade também dependente do estresse hídrico.
\end{itemize}
É importante ressaltar que a vegetação rasteira não precisa ser
vizinha à célula vazia para se espalhar. A função utilizada para a
probabilidade de semeadura para as árvores e vegetação rasteira se
encontra grafada na figura 1. Para valores mais baixos de estresse
dinâmico, as árvores possuem uma maior chance de se espalharem,
enquanto a vegetação rasteira possui uma probabilidade de disseminação
menor, mas que decai com menor intensidade em função do aumento do
estresse dinâmico. A modelagem da probabilidade de disseminação se
baseia em estudos anteriores feitos analisando a vegetação do Texas,
nos Estados Unidos~\cite{archer1988autogenic}.

Na execução deste modelo, cada avanço temporal corresponde a uma
estação de crescimento onde $\lambda$ e $\alpha$ são oriundos de uma
distribuição histórica desses parâmetros. Para tais parâmetros o
estresse hídrico dinâmico para cada vegetação (árvore, árvore em
desenvolvimento e vegetação rasteira) é calculado, e deste se computa
as taxas de morte e disseminação para cada qual. Os valores de
estresse hídrico, taxa de morte e taxa de disseminação são computados
novamente a cada iteração.

\section{Motivação}
É indubitável que as mudanças climáticas vêm causando imenso estrago
para diversos biomas, e dados revelam que a fragilidade dos
ecossistemas se encontra cada vez mais notável. Ao observar projeções
representantes da área ocupada por cada bioma, ano após ano,
observa-se um padrão claro de extinção catastrófica de diversas
espécies comuns e endêmicas. Sendo assim, estudos populacionais de
biodiversidade em ecossistemas se mostram cada vez mais necessários, e
na intenção de desbravar este terreno fértil e promissor, o presente
projeto se colocou como disposto para desenvolver tal tarefa, tendo
enfoque no cerrado brasileiro e toda sua relevância ambiental e
nacional.

\section{Objetivos}
Estudar métodos de sistema complexos aplicados a temáticas ambientais
e desenvolver modelagem via autômatos celulares da competição entre
vegetações rasteiras e arbóreas, utilizando fatores climáticos, como
frequência pluvial, e fatores ambientais, tais como informações acerca
da morfologia do substrato local. Visa-se aplicar o sistema para o
bioma cerrado, que abrange por volta de 24\% do território brasileiro,
segundo levantamento do INPE de 2019~\cite{vegetacao2019inpe}.

\section{Metodologia}
Os parâmetros da equação~\ref{eq:pontual} para a região do cerrado
brasileiro deverá ser obtida ao decorrer do projeto, junto aos
parâmetros estatísticos do modelo espacial para a região, afim de
possibilitar a ligação dos modelos. Decorrente disso, é calculada
também o estresse dinâmico, para os parâmetros do modelo pontual em
função da taxa de ocorrência de chuvas $\lambda$ para uma intensidade
média de chuvas $\alpha^{-1}$ constante e se aplica este resultado,
por fim, no modelo espacial.

Neste trabalho será utilizada a linguagem de programação Python para
implementar a simulação do modelo espacial descrito até então,
utilizando dados originários de estudos do Cerrado.

\section{Resultados}
Como resultado do nosso projeto, tivemos apenas um código estruturado
em python capaz de realizar a simulação do modelo espacial, como
entrada uma função de estresse hídrico arbitrária, em função do
tempo. O funcionamento do código se dá em algumas etapas, nas quais
são calculadas as probabilidades de ocorrência de morte e semeadura
dos tipos de vegetação para cata instante de tempo na simulação. O
modelo também permite a execução de várias iterações com a mesma
condição inicial, afim de possibilitar a análise do comportamento
médio do sistema, visto que ele é desenvolvido em cima de fenômenos
probabilisticos. Para as simulações no presente trabalho, foram
consideradas funções estimadas para o calculo das probabilidades em
função do estresse hídrico, como mostradas a seguir:

\begin{center}
$\sigma = e^{-\lambda/0.2}$\\
$p_{semeadura\_arvore}=-2\sigma + 0.8$\\
$p_{semeadura\_grama}=-3\sigma + 0.4$\\
$p_{morte\_arvore}=2^{6(\sigma-1)}$\\
$p_{morte\_muda}=2^{5.2(\sigma-1)}$\\
$p_{morte\_grama}=2^{5(\sigma-1)}$\\
\end{center}
onde $\sigma$ é o estresse hídrico, $\lambda$ a frequência de chuvas e p
as probabilidades de acontecimento de cada evento

O código em python é constituído de duas partes, a implementação e a
simulação, separados em dois arquivos. A implementação se dá pela
definição de uma classe, na qual possui os atributos e métodos
necessários e requisitados no arquivo de simulação. Portanto, a
simulação é um script que automatiza as simulações para um caso
especificado na mesma. As simulações podem e foram feitas de duas
maneiras: análise do modelo para uma função de estresse hídrico
específica, variável no temop e a análise de várias simulações
conjunta em função de estresses hídricos constantes. No segundo caso,
a ilustração dos automatos celulares não possuem muita importância,
pois a análise é apenas em proporções populacionais.

Para analizar o modelo mais detalhadamente, foram realizadas quatro
simulações, cada qual com funções diferentes para a frequência de
chuvas em função do tempo, sendo uma função crescente, decrescente,
constante e periódica. Para todas foi utilizada uma grade de 80x80 e
populações iniciais iguais.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/constante}
  \caption{Simulação para frequência de chuvas constante}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.65]{img/constantegrid}
  \caption{Disposição final da grade para frequência constante de
    chuvas. Verde, azul branco e bege representam respectivamente
    árvores, mudas, vazio e vegetação rasteira}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.65]{img/crescentegrid}
  \caption{Disposição final da grade para frequência crescente de
    chuvas. Verde, azul branco e bege representam respectivamente
    árvores, mudas, vazio e vegetação rasteira}

\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/crescente}
  \caption{Simulação para frequência de chuvas crescente}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/decrescente}
  \caption{Simulação para frequência de chuvas decrescente}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/periodico}
  \caption{Simulação para frequência de chuvas periódica}
\end{figure}


Utilizando uma grade de 80x80, foi realizada a simulação do sistema
para diferentes valores de frequência de chuva e grafados os valores
populacionais ao cabo de 200 iterações. Foi utilizada a média dos 15
ultimos valores de cada simulação; cada simulação se constitui do
valor médio populacional em cada instante de tempo em uma amostra de 6
outras simulações, dadas o nome de ``execução''.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/qtdfreq}
  \caption{Populações de equilíbrio em função da frequência de chuvas}
\end{figure}



\appendix
\appendixpage{}

\section{Código}
\subsection{simulacao.py}
\begin{minted}[fontsize=\footnotesize]{python}
import numpy as np
from statistics import mean
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from automata import *
import sympy as sm
import math

t = sm.symbols('t')
freqChuva = sm.Function('lambda')(t)
stress = sm.Function('sigma')(freqChuva)
stress = (math.e)**(-freqChuva/0.2)

def qtdPorEstresse(n, freqsChuvas, numIteracoes, condInicial, numExec):
    qtdFinal = {'A': [], 'M': [], 'V': [], 'G': []}
    for a in freqsChuvas:
        print('Calculando p/ freqChuva =', a)
        freqChuva = a + 0*t
        stress = (math.e)**(-freqChuva/0.2)
        tg = automata(n, stress, t, numIteracoes, condInicial = condInicial)
        tg.nExec(numExec)
        qtd = tg.qtdMedia
        print('Vazio final:', mean(qtd['V'][numIteracoes-15:numIteracoes]))
        for k in qtdFinal:
            qtdFinal[k].append(mean(qtd[k][numIteracoes-15:numIteracoes]))
            #tempo = tg.tempo
    
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_title('Populações finais após '+ str(numIteracoes) +
                 ' iterações em função da frequência de chuvas')
    ax.set_xlabel('Freq. chuvas (1/dia)')
    ax.set_ylabel('População final (%)')
    ax.plot(freqsChuvas, qtdFinal['A'], label='Arvores', color='green')
    ax.plot(freqsChuvas, qtdFinal['V'], label='Vazio', color='gray')
    ax.plot(freqsChuvas, qtdFinal['M'], label='Muda', color='blue')
    ax.plot(freqsChuvas, qtdFinal['G'], label='Grama', color='#CC881A')
    ax.legend()

    plt.show()

def qtdPorTempo(n, numIteracoes, condInicial, numExec):
    stress = (math.e)**(-freqChuva/0.2)
    tg = automata(n, stress, t, numIteracoes, condInicial = condInicial)
    tg.nExec(numExec)
    qtd = tg.qtdMedia
    tempo = tg.tempo
    matriz = np.array(tg.gridToInt())

    chuvaVals = []
    for i in tempo:
        chuvaVals.append(freqChuva.evalf(subs={t: i}))

    fig, ax = plt.subplots(3)
    fig2, ax2 = plt.subplots()

    ax[0].grid(True)
    ax[0].set_xlabel('Tempo (ano)')
    ax[0].set_ylabel('Quantidade (%)')
    ax[0].plot(tempo, qtd['A'], label='Arvores', color='green')
    ax[0].plot(tempo, qtd['V'], label='Vazio', color='gray')
    ax[0].plot(tempo, qtd['M'], label='Muda', color='blue')
    ax[0].plot(tempo, qtd['G'], label='Grama', color='#CC881A')
    ax[0].legend()

    ax[1].grid(True)
    ax[1].set_xlabel('Tempo (ano)')
    ax[1].set_ylabel('Freq. Chuvas (1/dia)')
    ax[1].plot(tempo, chuvaVals, label="Frequência de chuvas", color='red')
    ax[1].legend()

    ax[2].grid(True)
    ax[2].set_xlabel('Qtd. Arvores (%)')
    ax[2].set_ylabel('Qtd. Gramas (%)')
    ax[2].plot(qtd['A'], qtd['G'], color='purple')
    ax[2].legend()

    ax2.matshow(matriz, cmap = colors.ListedColormap(['#EBDBB2', '#B8BB26', '#83A598', '#CC881A']))
    ax2.axis(False)
    plt.show()

# Parâmetros para a execução das simulações, alterados de acordo com a 
# simulação a ser realizada
############################################################################

#n = 80
#condInicial = {'V': 25, 'M': 25, 'G': 25, 'A': 25}
#numIteracoes = 200
#numExec = 1

#freqChuva = 1 - t/200
#qtdPorTempo(n, numIteracoes, condInicial, numExec)

#freqsChuvas = np.arange(0.1, 0.5, 0.005)
#qtdPorEstresse(n, freqsChuvas, numIteracoes, condInicial, numExec)
\end{minted}

\subsection{automato.py}
\begin{minted}[fontsize=\footnotesize]{python}
from statistics import mean
from colorama import Back, Fore, Style
import random
import sympy as sm

# 'V' vazia
# 'A' arvore
# 'G' grama
# 'M' muda

# Grade 100x100
# Gera uma grade aleatória

class automata:


    def __init__(self, n, stress, t, numIteracoes, preserveProbs = False,
                 condInicial = {'V': 25, 'A': 25, 'G': 25, 'M': 25}):
        self.instante = 0
        self.probScale = 100

        self.stress = stress
        self.condInicial = condInicial
        self.t = t
        self.n = n
        self.numIteracoes = numIteracoes
        self.tempo = range(numIteracoes+1)
        self.funcSpreadArvore = sm.Function('gamma_arvore')(stress)
        self.funcSpreadGrama = sm.Function('gamma_grama')(stress)
        self.funcMorteArvore = sm.Function('eta_arvore')(stress)
        self.funcMorteGrama = sm.Function('eta_grama')(stress)
        self.funcMorteMuda = sm.Function('eta_muda')(stress)
    
        self.funcSpreadArvore = -2*(stress**3) + 0.8
        self.funcSpreadGrama = -3*(stress**3) + 0.4
        self.funcMorteArvore = 2**(6*(stress-1))
        self.funcMorteGrama = 2**(5*(stress-1))
        self.funcMorteMuda = 2**(5.2*(stress-1))

        if not preserveProbs:
            self.probSpreadArvore = []
            self.probSpreadGrama = []
            self.probMorteArvore = []
            self.probMorteGrama = []
            self.probMorteMuda = []
            self.probsCalc()

        self.grid = self.randomGrid(n, condInicial)
        self.genMudaAgeGrid()
        self.qtd = {'A': [], 'M': [], 'V': [], 'G': []}
        self.countCells()

    # executar isso e guardar tudo na memória, reutilizar pra implementação do numExec
    def probsCalc(self):
        print('Calculando probabilidades...')
        probScale = self.probScale
        t = self.t
        for instante in self.tempo:
            self.probSpreadArvore.append(int((probScale) * self.funcSpreadArvore.evalf(subs={t: instante})))
            self.probSpreadGrama.append(int((probScale) * self.funcSpreadGrama.evalf(subs={t: instante})))
            self.probMorteArvore.append(int((probScale) * self.funcMorteArvore.evalf(subs={t: instante})))
            self.probMorteGrama.append(int((probScale) * self.funcMorteGrama.evalf(subs={t: instante})))
            self.probMorteMuda.append(int((probScale) * self.funcMorteMuda.evalf(subs={t: instante})))
        print('Probabilidades calculadas')
        
    def genMudaAgeGrid(self):
        self.mudaAgeGrid = []
        for i in range(self.n):
            self.mudaAgeGrid.append([])
            for j in range(self.n):
                if self.grid[i][j] == 'M':
                    self.mudaAgeGrid[i].append(0)
                else:
                    self.mudaAgeGrid[i].append(-1)

    def updateMudaAgeGrid(self):
        for i in range(self.n):
            for j in range(self.n):

                if self.grid[i][j] == 'M':
                    if self.mudaAgeGrid[i][j] == 5:
                        self.mudaAgeGrid[i][j] = -1
                        self.grid[i][j] = 'A'
                    else:
                        self.mudaAgeGrid[i][j] += 1

                else:
                    self.mudaAgeGrid[i][j] = -1
                

    # probs = {V': probV...} probV = float duas casas decimais
    @staticmethod
    def randomGrid(n, condInicial):
        grid = []
        for i in range(n):
            grid.append([])
            for j in range(n):
                grid[i].append(random.sample(['V','A','G','M'], k=1,
                                             counts=[condInicial['V'],
                                                     condInicial['A'],
                                                     condInicial['G'],
                                                     condInicial['M']])[0])
        return grid
    
    def gridToInt(self):
        intGrid = []
        for i in range(self.n):
            intGrid.append([])
            for j in range(self.n):
                if self.grid[i][j] == 'V':
                    intGrid[i].append(0)
                elif self.grid[i][j] == 'A':
                    intGrid[i].append(1)
                elif self.grid[i][j] == 'M':
                    intGrid[i].append(2)
                elif self.grid[i][j] == 'G':
                    intGrid[i].append(3)                    
        return intGrid

    def randomNeighbor(self, i, j):
        random_i = i + random.choice([-2, -1, 1, 2])
        random_j = j + random.choice([-2, -1, 1, 2])

        if random_i >= self.n:
            random_i = random_i % self.n

        if random_j >= self.n:
            random_j = random_j % self.n

        return self.grid[random_i][random_j]
        
    def evalCell(self, i, j):            
        inst = self.instante
        cell = self.grid[i][j]
        if cell == 'V':
            neighborCell = self.randomNeighbor(i, j)

            if neighborCell == 'A':
                s = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadArvore[inst],
                                                       self.probSpreadArvore[inst]])[0]
                if s == 1:
                    return 'M'
                else:
                    p = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadGrama[inst],
                                                           self.probSpreadGrama[inst]])[0]
                    if p == 1:
                        return 'G'
                    else:
                        return 'V'

            else:
                s = random.sample([0, 1], k=1, counts=[self.probScale - self.probSpreadGrama[inst],
                                                       self.probSpreadGrama[inst]])[0]
                if s == 1:
                    return 'G'
                else:
                    return 'V'

        elif cell == 'G':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteGrama[inst],
                                                   self.probMorteGrama[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'G'
                
        elif cell == 'M':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteMuda[inst],
                                                   self.probMorteMuda[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'M'

        elif cell == 'A':
            s = random.sample([0, 1], k=1, counts=[self.probScale - self.probMorteArvore[inst],
                                                   self.probMorteArvore[inst]])[0]
            if s == 1:
                return 'V'
            else:
                return 'A'

    def iterate(self):
        posGrid = []
        for i in range(self.n):
            posGrid.append([])
            for j in range(self.n):
                posGrid[i].append(self.evalCell(i, j))
        self.grid = posGrid
        self.instante += 1
        self.updateMudaAgeGrid()
        self.countCells()

    def nExec(self, numExec):
        qtdN = []
        for a in range(numExec):
            self.__init__(self.n, self.stress, self.t, self.numIteracoes,
                          preserveProbs = True, condInicial = self.condInicial)
            print(str(a+1) + 'a', 'execução...')
            for i in range(self.numIteracoes):
                self.iterate()
            qtdN.append(self.qtd)
            print('Finalizada')


        if numExec != 1:
            self.qtdMedia = {'A': [], 'M': [], 'V': [], 'G': []}
            print('Calculando as médias...')
            for k in self.qtd:
                for t in self.tempo:
                    temp = []
                    for a in range(numExec):
                        temp.append(qtdN[a][k][t])
                    self.qtdMedia[k].append(mean(temp))
            print('Médias calculadas')
        else:
            self.qtdMedia = qtdN[0]

    def countCells(self):
        for k in self.qtd:
            self.qtd[k].append(0)

        for i in range(self.n):
            for j in range(self.n):
                self.qtd[self.grid[i][j]][self.instante] += 1

        n2 = (self.n ** 2) / 100
        for k in self.qtd:
            self.qtd[k][self.instante] /= n2

    


    def __str__(self):
        printableGrid = ''
        for i in range(self.n):
            for j in range(self.n):
                if self.grid[i][j] == 'A':
                    printableGrid += Fore.GREEN + Back.GREEN + 'A '
                elif self.grid[i][j] == 'M':
                    printableGrid += Fore.BLUE + Back.BLUE + 'M '
                elif self.grid[i][j] == 'G':
                    printableGrid += Fore.YELLOW + Back.YELLOW + 'G '
                elif self.grid[i][j] == 'V':
                    printableGrid += Fore.WHITE + Back.WHITE + 'V '
            printableGrid += Style.RESET_ALL + '\n'

        printableGrid += Fore.GREEN + 'Árvores: ' + str(self.qtd['A'][self.instante]) + '%\n'
        printableGrid += Fore.BLUE + 'Mudas: ' + str(self.qtd['M'][self.instante]) + '%\n'
        printableGrid += Fore.YELLOW + 'Gramas: ' + str(self.qtd['G'][self.instante]) + '%\n'
        printableGrid += Fore.WHITE + 'Vazio: ' + str(self.qtd['V'][self.instante]) + '%\n'

        return printableGrid
\end{minted}


\bibliography{ref}
\bibliographystyle{unsrt}

\end{document}
